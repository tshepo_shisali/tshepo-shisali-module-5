// ignore_for_file: prefer_const_constructors

import 'dart:io';

import 'package:flutter/material.dart';

class HomeItemsOne extends StatelessWidget {
  const HomeItemsOne({required this.name, required this.sub_name, required this.image, required this.press});

  final String sub_name;
  final String name;
  final String image;
  final VoidCallback press;


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
        child: Card(
          child: Row(
            
            children: [
              Expanded(
                child: Column(
                  children: [
                    Text(
                      name, style: Theme.of(context).textTheme.headline4,
                    ),  
                     Text(
                      sub_name, style: Theme.of(context).textTheme.headline5,
                    ),                
                  ]
                ),
              ),
              Image.asset("assets/" + image),
            ],
          ),
        )
    );
  }
}


//     Expanded(
//               child: Padding(
//                 padding: EdgeInsets.symmetric(
//                   horizontal: 10,
//         ),
//          child: HomeItemsOne(
//            name: 'flu season',
//               sub_name: 'order',
//                image: 'assets/image1.png', 
//                press:  () => 
//                           Navigator.push(context, MaterialPageRoute(
//                             builder: (context) => DetailsScreen(),
//                             )),),
//  ),
//             ),