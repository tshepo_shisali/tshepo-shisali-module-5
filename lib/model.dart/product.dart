
class Product {
  final String image, name;
  int id;

  Product({ required this.id, required this.name, required this.image });
  
}

List<Product> products = [
  Product(
    id: 1,
    name: 'Skin' ,
    image: 'assets/skin.png',
    ),
  Product(
   id: 2,
    name: 'Hair' ,
    image: 'assets/image1.png',
    ),
  Product(
   id: 3,
    name: 'Flu' ,
    image: 'assets/skin.png',
    ),
  Product(
   id: 4,
    name: 'Baby' ,
    image: 'assets/image1.png',
    ),
  Product(
   id: 5,
    name: 'Tea' ,
    image: 'assets/skin.png',
    ),
  
];