import 'package:flutter/material.dart';

ThemeData theme(){
  return ThemeData(
    primaryColor: const Color(0xff01021d),
    scaffoldBackgroundColor: const Color(0xff120052),
    backgroundColor: const Color(0xff00002f), 
    //Color(0xff42008d),

    textTheme: const TextTheme(
      
      headline1: TextStyle(
        color: Colors.white,
        fontSize: 30,
        fontWeight: FontWeight.bold,
      ),
      headline2: TextStyle(
        color: Colors.white,
        fontSize: 25,
        fontWeight: FontWeight.bold,
      ),
      headline3: TextStyle(
        color: Colors.white,
        fontSize: 20,
        fontWeight: FontWeight.bold,
      ),
       headline4: TextStyle(
        color: Colors.white,
        fontSize: 15,
        fontWeight: FontWeight.bold,
      ), 
      headline5: TextStyle(
        color: Colors.white,
        fontSize: 15,
        fontWeight: FontWeight.normal,
      ), 
      headline6: TextStyle(
        color: Color(0xff734094),
        fontSize: 15,
        fontWeight: FontWeight.normal,
      ),
    )

  );

}