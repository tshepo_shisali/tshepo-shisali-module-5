// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class Consult extends StatefulWidget {
  const Consult({Key? key}) : super(key: key);

  @override
  _ConsultState createState() => _ConsultState();
}

class _ConsultState extends State<Consult> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body: Center(
          child: Text('Consult',
            style: TextStyle(
              fontSize: 30, fontWeight: FontWeight.bold,
              color: Colors.blue,
            ),)
      ),
    );
  }
}
