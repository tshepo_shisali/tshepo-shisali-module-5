// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:med_it/model.dart/account.dart';

class Profile extends StatefulWidget {
  const Profile({ Key? key }) : super(key: key);

  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        
        title: Text('Profile', style: Theme.of(context).textTheme.headline3,),
        backgroundColor: Theme.of(context).primaryColor,

        actions: <Widget> [
          IconButton(
           icon: Icon(Icons.search, color: Colors.white,),
           iconSize: 30.0,
           onPressed: (){},
          ),
           IconButton(
           icon: Icon(Icons.account_circle_outlined, color: Colors.white),
           iconSize: 30.0,
           onPressed: (){},
          ),
        ]
        
      ),
      body: SingleChildScrollView(
             child: Account(),
      ),
      
    );
  }
}