// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:med_it/model.dart/box_card_home.dart';
import 'package:med_it/model.dart/extra_card.dart';
import 'package:med_it/screens/Categories/details.dart';
import 'package:med_it/model.dart/product.dart';
import 'package:med_it/screens/profile_widget/personal_details.dart';

class Home extends StatefulWidget {
  
  
  Home({ Key? key,}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        
        title: Text('Home', 
        style:  Theme.of(context).textTheme.headline3,),
        backgroundColor: Theme.of(context).primaryColor,

        actions: <Widget> [
          IconButton(
           icon: Icon(Icons.search, color: Colors.white),
           iconSize: 30.0,
           onPressed: (){},
          ),
           IconButton(
           icon: Icon(Icons.account_circle_outlined, color: Colors.white),
           iconSize: 30.0,
           onPressed: (){
              Navigator.push(context,
             MaterialPageRoute(builder: (context)=> Personal_details()));
           },
          ),
        ]
        
      ),
      body: Column(
        
          crossAxisAlignment: CrossAxisAlignment.start,
          
            children: [
              Padding(
              padding: EdgeInsets.all(10
        ),
        child: Text('Hi, user', style:  Theme.of(context).textTheme.headline3,
              ),
            
        ),
 Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 10,
        ),
         child: Text('Please edit your account in the profile page',
          style:  Theme.of(context).textTheme.headline5,
              ),
 ),
        
             
              Expanded(
                child: Padding(
                 padding: EdgeInsets.symmetric(
                   horizontal: 20.0,
                   vertical: 20.0,
                   
                 ),
                 child: GridView.builder(
                   
                   itemCount: 5,
                   gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                     mainAxisSpacing: 10.0,
                     crossAxisCount: 2,
                     
                     crossAxisSpacing: 15.0,
                     
                     childAspectRatio: 0.75,
                     ),
                      itemBuilder: (context, index) => Category(
                        product: products[index],
                         press: () => 
                        Navigator.push(context, MaterialPageRoute(
                          builder: (context) => DetailsScreen(),
                          ))
                      ))),
                  
                )
                
             
            ],

            
          ),

          
        );
              

  }
}
