// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors, use_key_in_widget_constructors, avoid_print

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:med_it/screens/bottom.dart';
import 'package:med_it/screens/register.dart';

class Login extends StatefulWidget {

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {

  bool _isHidden = true;
  TextEditingController email =  TextEditingController();

  TextEditingController pass =  TextEditingController();

  void _passwordView(){
    setState(() {
      _isHidden = !_isHidden;
    });
  }
  


  @override
  Widget build(BuildContext context){
    
    return Scaffold(
      
      // backgroundColor: Color(0xff00002f),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 90.0,
            vertical: 100.0,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
               
               Image.asset("assets/image2.png"),
               Padding(padding: EdgeInsets.only(bottom: 20)),
               TextField(
               controller: email,
                 style: Theme.of(context).textTheme.headline5,
                decoration: InputDecoration(
                labelText: 'Email',
                labelStyle: Theme.of(context).textTheme.headline3,
                 border: OutlineInputBorder(),
                 ),
               ),
               TextField(
               controller: pass,
                 style: Theme.of(context).textTheme.headline5,
                 obscureText: _isHidden,
                decoration: InputDecoration(
                labelText: 'Password',
                  suffix: InkWell(
                    onTap: _passwordView,
                    child: Icon(
                      _isHidden
                          ? Icons.visibility
                          : Icons.visibility_off,
                    ), ),
                labelStyle: Theme.of(context).textTheme.headline3,
                 border:  OutlineInputBorder(),
                 ),
               ),
              
                Padding(padding: EdgeInsets.only(bottom: 20)),
               Container(
                width: MediaQuery.of(context).size.width,
                height: 50.0,
                margin: const EdgeInsets.fromLTRB(0, 50, 0, 28),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(50)),
                 child: ElevatedButton(
                    style: ButtonStyle(
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(RoundedRectangleBorder(borderRadius: BorderRadius.circular(50) )),
                    backgroundColor: MaterialStateProperty.all<Color>(Theme.of(context).primaryColor),
                   ),
                   child: Text('Login',
                     style:  Theme.of(context).textTheme.headline1,
                   ),

                   onPressed: (){
                    FirebaseAuth.instance.signInWithEmailAndPassword(
                      email: email.text, password: pass.text).then((value) {
                        Navigator.push(context,
                         MaterialPageRoute(builder: (context)=> Bottom_navigator_page(email: email.toString())));
                      }).onError(
                        (error, stackTrace)  {
                          print("error ${error.toString()}");
                        });
                     

                   },
              ),
               ),
               Container(
                 height: 40,
                 color: Colors.transparent,
                 child: TextButton(
                   child: Text('Dont have an account',
                     style: Theme.of(context).textTheme.headline4,
                   ),

                   onPressed: (){
                    
                     Navigator.push(context,
                         MaterialPageRoute(builder: (context)=> Register()));

                   },
              ),
               ),
               
                 ],
          ),
        ),
      ),

    
    );
  }
}



 