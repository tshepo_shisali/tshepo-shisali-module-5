// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';


class ProfilePic extends StatelessWidget {
  const ProfilePic({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 20),
      child: SizedBox(
        
        height: 85,
        width: 85,
        child: Stack(
          fit: StackFit.expand,
          children:  [
            CircleAvatar(
              backgroundImage: AssetImage('assets/skin.png'),
              backgroundColor: Colors.transparent,
            ),
            Positioned(
              right: -12,
              bottom: 0,
              child: 
            SizedBox(
              height: 46,
              width: 46,
              
              child: TextButton(
                
                onPressed: () {},
                 child: Icon(Icons.photo),
                 ),
            )
            )
          ],
        ),
      ),
    );
  }
}