// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for macos - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyC2o5pyDMlW0BgSsefyQdXKjNs-ZJxlYsA',
    appId: '1:426781903231:web:63cf8cc26b902e756f324a',
    messagingSenderId: '426781903231',
    projectId: 'med-it-8a4e2',
    authDomain: 'med-it-8a4e2.firebaseapp.com',
    storageBucket: 'med-it-8a4e2.appspot.com',
    measurementId: 'G-WTCS29B54H',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyCU0SVgG_hR8otg64x2rzPsFIzYxg6qfZo',
    appId: '1:426781903231:android:a835d59805fa399f6f324a',
    messagingSenderId: '426781903231',
    projectId: 'med-it-8a4e2',
    storageBucket: 'med-it-8a4e2.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyDkn2MJqdNyLipdLwro04kIaY6cGty2OyU',
    appId: '1:426781903231:ios:deb6cb9cfd14a7ad6f324a',
    messagingSenderId: '426781903231',
    projectId: 'med-it-8a4e2',
    storageBucket: 'med-it-8a4e2.appspot.com',
    iosClientId: '426781903231-sk1athpv9675eqp6sg7dso7kqlabh3hg.apps.googleusercontent.com',
    iosBundleId: 'com.example.medIt',
  );
}
